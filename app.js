/**
 * Entry point of the service provider(FS) demo app.
 * @see @link{ https://partenaires.franceconnect.gouv.fr/fcp/fournisseur-service# }
 */
import express from 'express';
import logger from 'morgan';
import session from 'express-session';
import sessionstore from 'sessionstore';
import bodyParser from 'body-parser';
import crypto from 'crypto';
import config from './config';


import {
  getUser,
  oauthLoginAuthorize,
  oauthLoginCallback,
  oauthLogoutAuthorize,
  oauthLogoutCallback,
} from './controllers/oauthAuthentication';
import {
  oauthDataAuthorize,
  oauthDataCallback,
} from './controllers/oauthData';
import {
  oauthTracksAuthorize,
  oauthTracksCallback,
} from './controllers/oauthTracks';
import { callbackParamsValidatorMiddleware } from './validators/callbackParams';

const app = express();

// Note this enable to store user session in memory
// As a consequence, restarting the node process will wipe all sessions data
app.use(session({
  store: sessionstore.createSessionStore(),
  secret: 'demo secret', // put your own secret
  cookie: {},
  saveUninitialized: true,
  resave: true,
}));

if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
}

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'ejs');

// pass the user data from session to template global variables
app.use((req, res, next) => {
  res.locals.user = req.session.user;
  res.locals.data = req.session.data;
  next();
});

// define variable globally for the footer
app.locals.franceConnectKitUrl = `${config.FC_URL}${config.FRANCE_CONNECT_KIT_PATH}`;

app.get('/', (req, res) => res.render('pages/home'));

app.get('/login', (req, res) => {
  // const scopesSelectedByDefault = `${config.MANDATORY_SCOPES} ${config.FC_SCOPES}`.split(' ');
  // eslint-disable-next-line max-len
  // return res.status(200).render('pages/login', { scopesSelectedByDefault, scopesFamilies: SCOPES_GROUPS });
  const claims = false;
  const eidasLevel = 'eidas1';
  const myScopes = {
    eidasLevel: 'eidas1',
    scope_openid: 'on',
    scope_given_name: 'on',
    scope_family_name: 'on',
    scope_gender: 'on',
    scope_preferred_username: 'on',
    scope_birthdate: 'on',
    claims: '{"id_token":{"amr":{"essential":true}}}',
    x: '77',
    y: '21',
  };
  const scopes = Object.keys(myScopes)
    .filter(key => key.startsWith('scope_'))
    .map(scope => scope.split('scope_').pop())
    .join(' ');

  // eslint-disable-next-line no-console
  console.log('**********************************', req.body);

  // eslint-disable-next-line no-console
  console.log('++++++++++++++++++++++++++++++++++', scopes);
  // const scopes = "openid given_name family_name gender preferred_username birthdate"
  const query = {
    scope: scopes,
    redirect_uri: `${config.FS_URL}${config.LOGIN_CALLBACK_FS_PATH}`,
    response_type: 'code',
    client_id: config.AUTHENTICATION_CLIENT_ID,
    state: `state${crypto.randomBytes(32).toString('hex')}`,
    nonce: `nonce${crypto.randomBytes(32).toString('hex')}`,
  };

  if (claims) {
    query.claims = claims;
  }

  // Save requested scopes in the session
  req.session.scopes = scopes;

  if (eidasLevel) {
    query.acr_values = eidasLevel;
  }

  const url = `${config.FC_URL}${config.AUTHORIZATION_FC_PATH}`;
  const params = new URLSearchParams(query).toString();
  return res.redirect(`${url}?${params}`);
});

app.post('/login-authorize', oauthLoginAuthorize);

app.get('/login-callback', callbackParamsValidatorMiddleware, oauthLoginCallback);

app.get('/logout', oauthLogoutAuthorize);

app.get('/logout-callback', oauthLogoutCallback);

app.get('/data-authorize', oauthDataAuthorize);

app.get('/data-callback', callbackParamsValidatorMiddleware, oauthDataCallback);

app.get('/tracks-authorize', oauthTracksAuthorize);

app.get('/tracks-callback', callbackParamsValidatorMiddleware, oauthTracksCallback);

app.get('/user', getUser);

// Setting app port
const port = process.env.PORT || '3000';
// Starting server
const server = app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`\x1b[32mServer listening on http://localhost:${port}\x1b[0m`);
});

export default server;
